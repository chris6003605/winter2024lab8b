import java.util.Random;
public class Board {


    private Tile[][] grid;

    public Board() {
        final int LEN = 5;
        this.grid = new Tile[LEN][LEN];
		 Random rng = new Random();
        for (int rowIndex = 0; rowIndex < grid.length; rowIndex++) {
			int randIndex = rng.nextInt(5);
            for (int colIndex = 0; colIndex < grid[rowIndex].length; colIndex++) {
                if(colIndex == randIndex){
					this.grid[rowIndex][colIndex] = Tile.HIDDEN_WALL;
				}
				else{
				this.grid[rowIndex][colIndex] = Tile.BLANK;
				}
			}
        }
    }

    public int placeToken(int row, int col) {
        if (!(row < this.grid.length && row >= 0 && col < this.grid.length && col >= 0)){
            return -2;
        }
		else if(this.grid[row][col].equals(Tile.CASTLE) ||this.grid[row][col].equals(Tile.WALL))
        return -1;
		else if(this.grid[row][col].equals(Tile.HIDDEN_WALL)){
			return 1;
		}
		else{
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
    }
	
	public String toString() {
		String output = "";
        output += "  0 1 2 3 4\n";
		for (int rowIndex = 0; rowIndex < grid.length; rowIndex++) {
            output += rowIndex + " ";
            for (int colIndex = 0; colIndex < grid[rowIndex].length; colIndex++) {
                output += this.grid[rowIndex][colIndex].getName();

                if (colIndex != this.grid[rowIndex].length - 1) {
                    output += " ";
                }
				}
        output += "\n";
		}
    return output;
	}
}
        
		
		
		
		
		
	

	

   

   