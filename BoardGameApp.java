import java.util.Scanner;
public class BoardGameApp{
	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in); 
		Board board = new Board();
		
		System.out.println("Welcome to Game!");
		int numCastles =7;
		int turns =0;
		while(turns < 8){
			System.out.println(board);
			System.out.println("Input row and col");
			int row = Integer.parseInt(input.nextLine());
			int col = Integer.parseInt(input.nextLine());
			
			while(board.placeToken(row, col) < 0){
				System.out.println("Input valid num from 0-4");
				row = Integer.parseInt(input.nextLine());
				col = Integer.parseInt(input.nextLine());
			}
			
			if(board.placeToken(row, col) == 1){
				System.out.println("There is a wall at that position");
				turns++;
			}
			else if(board.placeToken(row, col) == 0){
				System.out.println("Castle Tile was succesfully placed");
				turns++;
				numCastles--;
			}
			else{
				turns++;
			}
		}
		System.out.println(board);
		if(numCastles == 0){
			System.out.println("You Have Won!");
		}
		else{
		System.out.println("You Have Lost!");
		}
	}
}